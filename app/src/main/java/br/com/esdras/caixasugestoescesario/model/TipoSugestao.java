package br.com.esdras.caixasugestoescesario.model;

import java.io.Serializable;
import java.util.Objects;

public class TipoSugestao implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String tipo;

    public TipoSugestao(int id, String tipo) {
        this.id = id;
        this.tipo = tipo;
    }

    public TipoSugestao() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TipoSugestao that = (TipoSugestao) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "TipoSugestao{" +
                "id=" + id +
                ", tipo='" + tipo + '\'' +
                '}';
    }
}
