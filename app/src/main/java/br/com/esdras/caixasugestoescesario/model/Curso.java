package br.com.esdras.caixasugestoescesario.model;

import java.io.Serializable;
import java.util.Objects;

public class Curso implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String curso;

    public Curso(int id, String curso) {
        this.id = id;
        this.curso = curso;
    }

    public Curso() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Curso that = (Curso) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Curso{" +
                "id=" + id +
                ", curso='" + curso + '\'' +
                '}';
    }
}
